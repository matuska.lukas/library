public class Main {

    public static void main(String[] args) {

        System.out.println();
        System.out.println("### OBJECTS ###");

        Library knihovnicka = new Library(0, "Nikdo");
        System.out.println(knihovnicka);

        Author kovy = new Author (0, "Karel", "Kovář");
        System.out.println(kovy);

        Author albatros = new Author (0, "Albatros", " ");
        System.out.println(kovy);

        Book ovsem = new Book(0, "OVŠEM", 500, 2017, kovy);
        System.out.println(ovsem);

        Magazine abicko = new Magazine(0, "ABC", 25, 2019, 3, albatros);
        System.out.println(abicko);

        System.out.println();
        System.out.println("### BOOKS ###");
        System.out.println("## Add new book over method");
        System.out.println("before: " + knihovnicka.getBooks());
        System.out.println("result: " + knihovnicka.newBook(1, "Bylo nás pět!", 222, 1999, albatros));
        System.out.println("after: " + knihovnicka.getBooks());

        System.out.println();
        System.out.println("## Add predefined object book");
        System.out.println("before: " + knihovnicka.getBooks());
        System.out.println("result: " + knihovnicka.addBook(ovsem));
        System.out.println("after: " + knihovnicka.getBooks());

        System.out.println();
        System.out.println("## Borrow object book");
        System.out.println("before: " + knihovnicka.getBooksRentals());
        System.out.println("result: " + knihovnicka.borrowBook(ovsem));
        System.out.println("after: " + knihovnicka.getBooksRentals());

        System.out.println();
        System.out.println("### MAGAZINES ###");
        System.out.println("## New magazine over method");
        System.out.println("before: " + knihovnicka.getMagazines());
        System.out.println("result: " + knihovnicka.newMagazine(1, "Computer", 30, 2019, 10, albatros));
        System.out.println("after: " + knihovnicka.getMagazines());

        System.out.println();
        System.out.println("## New magazine over object");
        System.out.println("before: " + knihovnicka.getMagazines());
        System.out.println("result: " + knihovnicka.addMagazine(abicko));
        System.out.println("after: " + knihovnicka.getMagazines());

        System.out.println();
        System.out.println("## Borrow predefined object magazine");
        System.out.println("before: " + knihovnicka.getMagazinesRentals());
        System.out.println("result: " + knihovnicka.borrowMagazine(abicko));
        System.out.println("after: " + knihovnicka.getMagazinesRentals());
    }
}
