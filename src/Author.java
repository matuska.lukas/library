public class Author {
    private int id;
    private String forename;
    private String surname;

    Author(int id, String forename, String surname) {
        this.id = id;
        this.forename = forename;
        this.surname = surname;
    }

    @Override
    public String toString(){
        return "Author(id=" + this.id +
                ", forename=\'" + this.forename + "\'" +
                ", surname=\'" + this.surname + "\'" +
                "); ";
    }
}
