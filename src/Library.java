import java.util.ArrayList;

public class Library {
    private int id;
    private String owner;
    private ArrayList<Book> books;
    private ArrayList<Object> booksRentals;
    private ArrayList<Magazine> magazines;
    private ArrayList<Object> magazinesRentals;

    Library (int id, String owner){
        this.id = id;
        this.owner = owner;
        this.books = new ArrayList<Book>();
        this.booksRentals = new ArrayList<Object>();
        this.magazines = new ArrayList<Magazine>();
        this.magazinesRentals = new ArrayList<Object>();
    }

    @Override
    public String toString(){
        return "Library(id=\'" + this.id + "\'" +
                ", owner=\'" + this.owner + "\'" +
                ", books=\'" + this.books + "\'" +
                ", booksRentals=" + this.booksRentals +
                ", magazines=\'" + this.magazines + "\'" +
                ", magazinesRentals=" + this.magazinesRentals +
                "); ";
    }

    /* ****************
    *  Manage book(s)
    *  ****************
    */

    public ArrayList getBooks(){
        return this.books;
    }


    public boolean newBook(int id, String name, int cost, int yearOfPrint, Author author) {
        return this.addBook(new Book(id, name, cost, yearOfPrint, author));
    }

    public boolean addBook(Book book){
        if (this.books.contains(book)){
            return false;
        }else{
            this.books.add(book);
            return true;
        }
    }

    public boolean removeBook(Book book){
        if (this.books.contains(book)){
            this.books.remove(book);
            return true;
        }else{
            return false;
        }
    }

    public boolean borrowBook(Book book){
        if (this.booksRentals.contains(book)){
            return false;
        }else{
            this.booksRentals.add(book);
            return true;
        }
    }

    public boolean returnBook(Book book){
        if (this.booksRentals.contains(book)){
            this.booksRentals.remove(book);
            return true;
        }else{
            return false;
        }
    }

    public ArrayList getBooksRentals(){
        return this.booksRentals;
    }

    /* ****************
     *  Manage magazine(s)
     *  ****************
     */

    public boolean newMagazine(int id, String name, int cost, int year, int number, Author author){
        return this.addMagazine(new Magazine(id, name, cost, year, number, author));
    }

    public boolean addMagazine(Magazine magazine){
        if (this.magazines.contains(magazine)){
            return false;
        }else{
            this.magazines.add(magazine);
            return true;
        }
    }

    public boolean borrowMagazine(Magazine magazine){
        if (this.magazinesRentals.contains(magazine)){
            return false;
        }else{
            this.magazinesRentals.add(magazine);
            return true;
        }
    }

    public boolean returnMagazine(Magazine magazine){
        if (this.magazinesRentals.contains(magazine)){
            this.magazinesRentals.remove(magazine);
            return true;
        }else{
            return false;
        }
    }

    public ArrayList getMagazines(){
        return this.magazines;
    }

    public ArrayList getMagazinesRentals(){
        return this.magazinesRentals;
    }
}
