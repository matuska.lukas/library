public class Magazine {
    private int id;
    private String name;
    private int cost;
    private int year;
    private int number;
    private Author author;

    Magazine (int id, String name, int cost, int year, int number, Author author) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.year = year;
        this.number = number;
        this.author = author;
    }

    @Override
    public String toString(){
        return "Magazine(id=" + this.id +
                ", name=\'" + this.name + "\'" +
                ", cost=" + this.cost +
                ", year=" + this.year +
                ", number=" + this.number +
                ", author=" + this.author +
                "); ";
    }
}
