public class Book {
    private int id;
    private String name;
    private int cost;
    private int yearOfPrint;
    private Author author;

    Book (int id, String name, int cost, int yearOfPrint, Author author) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.yearOfPrint = yearOfPrint;
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book(id=" + this.id +
                ", name=\'" + this.name + "\'" +
                ", cost=" + this.cost +
                ", yearOfPrint=" + this.yearOfPrint +
                ", author=" + this.author +
                "); ";
     }
}
